/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MonoTestModule } from '../../../test.module';
import { BandMemberDetailComponent } from 'app/entities/band-member/band-member-detail.component';
import { BandMember } from 'app/shared/model/band-member.model';

describe('Component Tests', () => {
  describe('BandMember Management Detail Component', () => {
    let comp: BandMemberDetailComponent;
    let fixture: ComponentFixture<BandMemberDetailComponent>;
    const route = ({ data: of({ bandMember: new BandMember(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MonoTestModule],
        declarations: [BandMemberDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BandMemberDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BandMemberDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bandMember).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
