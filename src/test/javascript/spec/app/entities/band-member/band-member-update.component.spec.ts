/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { MonoTestModule } from '../../../test.module';
import { BandMemberUpdateComponent } from 'app/entities/band-member/band-member-update.component';
import { BandMemberService } from 'app/entities/band-member/band-member.service';
import { BandMember } from 'app/shared/model/band-member.model';

describe('Component Tests', () => {
  describe('BandMember Management Update Component', () => {
    let comp: BandMemberUpdateComponent;
    let fixture: ComponentFixture<BandMemberUpdateComponent>;
    let service: BandMemberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MonoTestModule],
        declarations: [BandMemberUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BandMemberUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BandMemberUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BandMemberService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BandMember(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BandMember();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
