/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MonoTestModule } from '../../../test.module';
import { BandMemberComponent } from 'app/entities/band-member/band-member.component';
import { BandMemberService } from 'app/entities/band-member/band-member.service';
import { BandMember } from 'app/shared/model/band-member.model';

describe('Component Tests', () => {
  describe('BandMember Management Component', () => {
    let comp: BandMemberComponent;
    let fixture: ComponentFixture<BandMemberComponent>;
    let service: BandMemberService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MonoTestModule],
        declarations: [BandMemberComponent],
        providers: []
      })
        .overrideTemplate(BandMemberComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BandMemberComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BandMemberService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BandMember(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.bandMembers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
