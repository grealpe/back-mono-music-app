package co.edu.udistrital.web.rest;

import co.edu.udistrital.MonoApp;
import co.edu.udistrital.domain.Band;
import co.edu.udistrital.domain.BandMember;
import co.edu.udistrital.domain.Quotation;
import co.edu.udistrital.domain.Category;
import co.edu.udistrital.domain.Images;
import co.edu.udistrital.repository.BandRepository;
import co.edu.udistrital.service.BandService;
import co.edu.udistrital.service.dto.BandDTO;
import co.edu.udistrital.service.mapper.BandMapper;
import co.edu.udistrital.web.rest.errors.ExceptionTranslator;
import co.edu.udistrital.service.dto.BandCriteria;
import co.edu.udistrital.service.BandQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static co.edu.udistrital.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link BandResource} REST controller.
 */
@SpringBootTest(classes = MonoApp.class)
public class BandResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VIDEO = "AAAAAAAAAA";
    private static final String UPDATED_VIDEO = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Double DEFAULT_STAR = 0D;
    private static final Double UPDATED_STAR = 1D;

    @Autowired
    private BandRepository bandRepository;

    @Mock
    private BandRepository bandRepositoryMock;

    @Autowired
    private BandMapper bandMapper;

    @Mock
    private BandService bandServiceMock;

    @Autowired
    private BandService bandService;

    @Autowired
    private BandQueryService bandQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBandMockMvc;

    private Band band;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BandResource bandResource = new BandResource(bandService, bandQueryService);
        this.restBandMockMvc = MockMvcBuilders.standaloneSetup(bandResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Band createEntity(EntityManager em) {
        Band band = new Band()
            .name(DEFAULT_NAME)
            .video(DEFAULT_VIDEO)
            .description(DEFAULT_DESCRIPTION)
            .star(DEFAULT_STAR);
        return band;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Band createUpdatedEntity(EntityManager em) {
        Band band = new Band()
            .name(UPDATED_NAME)
            .video(UPDATED_VIDEO)
            .description(UPDATED_DESCRIPTION)
            .star(UPDATED_STAR);
        return band;
    }

    @BeforeEach
    public void initTest() {
        band = createEntity(em);
    }

    @Test
    @Transactional
    public void createBand() throws Exception {
        int databaseSizeBeforeCreate = bandRepository.findAll().size();

        // Create the Band
        BandDTO bandDTO = bandMapper.toDto(band);
        restBandMockMvc.perform(post("/api/bands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isCreated());

        // Validate the Band in the database
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeCreate + 1);
        Band testBand = bandList.get(bandList.size() - 1);
        assertThat(testBand.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBand.getVideo()).isEqualTo(DEFAULT_VIDEO);
        assertThat(testBand.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testBand.getStar()).isEqualTo(DEFAULT_STAR);
    }

    @Test
    @Transactional
    public void createBandWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bandRepository.findAll().size();

        // Create the Band with an existing ID
        band.setId(1L);
        BandDTO bandDTO = bandMapper.toDto(band);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBandMockMvc.perform(post("/api/bands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Band in the database
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = bandRepository.findAll().size();
        // set the field null
        band.setName(null);

        // Create the Band, which fails.
        BandDTO bandDTO = bandMapper.toDto(band);

        restBandMockMvc.perform(post("/api/bands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isBadRequest());

        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = bandRepository.findAll().size();
        // set the field null
        band.setDescription(null);

        // Create the Band, which fails.
        BandDTO bandDTO = bandMapper.toDto(band);

        restBandMockMvc.perform(post("/api/bands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isBadRequest());

        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBands() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList
        restBandMockMvc.perform(get("/api/bands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(band.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].video").value(hasItem(DEFAULT_VIDEO.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].star").value(hasItem(DEFAULT_STAR.doubleValue())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllBandsWithEagerRelationshipsIsEnabled() throws Exception {
        BandResource bandResource = new BandResource(bandServiceMock, bandQueryService);
        when(bandServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restBandMockMvc = MockMvcBuilders.standaloneSetup(bandResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restBandMockMvc.perform(get("/api/bands?eagerload=true"))
        .andExpect(status().isOk());

        verify(bandServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllBandsWithEagerRelationshipsIsNotEnabled() throws Exception {
        BandResource bandResource = new BandResource(bandServiceMock, bandQueryService);
            when(bandServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restBandMockMvc = MockMvcBuilders.standaloneSetup(bandResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restBandMockMvc.perform(get("/api/bands?eagerload=true"))
        .andExpect(status().isOk());

            verify(bandServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getBand() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get the band
        restBandMockMvc.perform(get("/api/bands/{id}", band.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(band.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.video").value(DEFAULT_VIDEO.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.star").value(DEFAULT_STAR.doubleValue()));
    }

    @Test
    @Transactional
    public void getAllBandsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name equals to DEFAULT_NAME
        defaultBandShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the bandList where name equals to UPDATED_NAME
        defaultBandShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllBandsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name in DEFAULT_NAME or UPDATED_NAME
        defaultBandShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the bandList where name equals to UPDATED_NAME
        defaultBandShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllBandsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where name is not null
        defaultBandShouldBeFound("name.specified=true");

        // Get all the bandList where name is null
        defaultBandShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllBandsByVideoIsEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where video equals to DEFAULT_VIDEO
        defaultBandShouldBeFound("video.equals=" + DEFAULT_VIDEO);

        // Get all the bandList where video equals to UPDATED_VIDEO
        defaultBandShouldNotBeFound("video.equals=" + UPDATED_VIDEO);
    }

    @Test
    @Transactional
    public void getAllBandsByVideoIsInShouldWork() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where video in DEFAULT_VIDEO or UPDATED_VIDEO
        defaultBandShouldBeFound("video.in=" + DEFAULT_VIDEO + "," + UPDATED_VIDEO);

        // Get all the bandList where video equals to UPDATED_VIDEO
        defaultBandShouldNotBeFound("video.in=" + UPDATED_VIDEO);
    }

    @Test
    @Transactional
    public void getAllBandsByVideoIsNullOrNotNull() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where video is not null
        defaultBandShouldBeFound("video.specified=true");

        // Get all the bandList where video is null
        defaultBandShouldNotBeFound("video.specified=false");
    }

    @Test
    @Transactional
    public void getAllBandsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where description equals to DEFAULT_DESCRIPTION
        defaultBandShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the bandList where description equals to UPDATED_DESCRIPTION
        defaultBandShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllBandsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultBandShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the bandList where description equals to UPDATED_DESCRIPTION
        defaultBandShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllBandsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where description is not null
        defaultBandShouldBeFound("description.specified=true");

        // Get all the bandList where description is null
        defaultBandShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllBandsByStarIsEqualToSomething() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where star equals to DEFAULT_STAR
        defaultBandShouldBeFound("star.equals=" + DEFAULT_STAR);

        // Get all the bandList where star equals to UPDATED_STAR
        defaultBandShouldNotBeFound("star.equals=" + UPDATED_STAR);
    }

    @Test
    @Transactional
    public void getAllBandsByStarIsInShouldWork() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where star in DEFAULT_STAR or UPDATED_STAR
        defaultBandShouldBeFound("star.in=" + DEFAULT_STAR + "," + UPDATED_STAR);

        // Get all the bandList where star equals to UPDATED_STAR
        defaultBandShouldNotBeFound("star.in=" + UPDATED_STAR);
    }

    @Test
    @Transactional
    public void getAllBandsByStarIsNullOrNotNull() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        // Get all the bandList where star is not null
        defaultBandShouldBeFound("star.specified=true");

        // Get all the bandList where star is null
        defaultBandShouldNotBeFound("star.specified=false");
    }

    @Test
    @Transactional
    public void getAllBandsByMembersIsEqualToSomething() throws Exception {
        // Initialize the database
        BandMember members = BandMemberResourceIT.createEntity(em);
        em.persist(members);
        em.flush();
        band.addMembers(members);
        bandRepository.saveAndFlush(band);
        Long membersId = members.getId();

        // Get all the bandList where members equals to membersId
        defaultBandShouldBeFound("membersId.equals=" + membersId);

        // Get all the bandList where members equals to membersId + 1
        defaultBandShouldNotBeFound("membersId.equals=" + (membersId + 1));
    }


    @Test
    @Transactional
    public void getAllBandsByQuotationsIsEqualToSomething() throws Exception {
        // Initialize the database
        Quotation quotations = QuotationResourceIT.createEntity(em);
        em.persist(quotations);
        em.flush();
        band.addQuotations(quotations);
        bandRepository.saveAndFlush(band);
        Long quotationsId = quotations.getId();

        // Get all the bandList where quotations equals to quotationsId
        defaultBandShouldBeFound("quotationsId.equals=" + quotationsId);

        // Get all the bandList where quotations equals to quotationsId + 1
        defaultBandShouldNotBeFound("quotationsId.equals=" + (quotationsId + 1));
    }


    @Test
    @Transactional
    public void getAllBandsByCategoriesIsEqualToSomething() throws Exception {
        // Initialize the database
        Category categories = CategoryResourceIT.createEntity(em);
        em.persist(categories);
        em.flush();
        band.addCategories(categories);
        bandRepository.saveAndFlush(band);
        Long categoriesId = categories.getId();

        // Get all the bandList where categories equals to categoriesId
        defaultBandShouldBeFound("categoriesId.equals=" + categoriesId);

        // Get all the bandList where categories equals to categoriesId + 1
        defaultBandShouldNotBeFound("categoriesId.equals=" + (categoriesId + 1));
    }


    @Test
    @Transactional
    public void getAllBandsByImagesIsEqualToSomething() throws Exception {
        // Initialize the database
        Images images = ImagesResourceIT.createEntity(em);
        em.persist(images);
        em.flush();
        band.addImages(images);
        bandRepository.saveAndFlush(band);
        Long imagesId = images.getId();

        // Get all the bandList where images equals to imagesId
        defaultBandShouldBeFound("imagesId.equals=" + imagesId);

        // Get all the bandList where images equals to imagesId + 1
        defaultBandShouldNotBeFound("imagesId.equals=" + (imagesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBandShouldBeFound(String filter) throws Exception {
        restBandMockMvc.perform(get("/api/bands?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(band.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].video").value(hasItem(DEFAULT_VIDEO)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].star").value(hasItem(DEFAULT_STAR.doubleValue())));

        // Check, that the count call also returns 1
        restBandMockMvc.perform(get("/api/bands/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBandShouldNotBeFound(String filter) throws Exception {
        restBandMockMvc.perform(get("/api/bands?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBandMockMvc.perform(get("/api/bands/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBand() throws Exception {
        // Get the band
        restBandMockMvc.perform(get("/api/bands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBand() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        int databaseSizeBeforeUpdate = bandRepository.findAll().size();

        // Update the band
        Band updatedBand = bandRepository.findById(band.getId()).get();
        // Disconnect from session so that the updates on updatedBand are not directly saved in db
        em.detach(updatedBand);
        updatedBand
            .name(UPDATED_NAME)
            .video(UPDATED_VIDEO)
            .description(UPDATED_DESCRIPTION)
            .star(UPDATED_STAR);
        BandDTO bandDTO = bandMapper.toDto(updatedBand);

        restBandMockMvc.perform(put("/api/bands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isOk());

        // Validate the Band in the database
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeUpdate);
        Band testBand = bandList.get(bandList.size() - 1);
        assertThat(testBand.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testBand.getVideo()).isEqualTo(UPDATED_VIDEO);
        assertThat(testBand.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testBand.getStar()).isEqualTo(UPDATED_STAR);
    }

    @Test
    @Transactional
    public void updateNonExistingBand() throws Exception {
        int databaseSizeBeforeUpdate = bandRepository.findAll().size();

        // Create the Band
        BandDTO bandDTO = bandMapper.toDto(band);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBandMockMvc.perform(put("/api/bands")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Band in the database
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBand() throws Exception {
        // Initialize the database
        bandRepository.saveAndFlush(band);

        int databaseSizeBeforeDelete = bandRepository.findAll().size();

        // Delete the band
        restBandMockMvc.perform(delete("/api/bands/{id}", band.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Band> bandList = bandRepository.findAll();
        assertThat(bandList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Band.class);
        Band band1 = new Band();
        band1.setId(1L);
        Band band2 = new Band();
        band2.setId(band1.getId());
        assertThat(band1).isEqualTo(band2);
        band2.setId(2L);
        assertThat(band1).isNotEqualTo(band2);
        band1.setId(null);
        assertThat(band1).isNotEqualTo(band2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BandDTO.class);
        BandDTO bandDTO1 = new BandDTO();
        bandDTO1.setId(1L);
        BandDTO bandDTO2 = new BandDTO();
        assertThat(bandDTO1).isNotEqualTo(bandDTO2);
        bandDTO2.setId(bandDTO1.getId());
        assertThat(bandDTO1).isEqualTo(bandDTO2);
        bandDTO2.setId(2L);
        assertThat(bandDTO1).isNotEqualTo(bandDTO2);
        bandDTO1.setId(null);
        assertThat(bandDTO1).isNotEqualTo(bandDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bandMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bandMapper.fromId(null)).isNull();
    }
}
