package co.edu.udistrital.web.rest;

import co.edu.udistrital.MonoApp;
import co.edu.udistrital.domain.BandMember;
import co.edu.udistrital.domain.User;
import co.edu.udistrital.domain.Band;
import co.edu.udistrital.repository.BandMemberRepository;
import co.edu.udistrital.service.BandMemberService;
import co.edu.udistrital.service.dto.BandMemberDTO;
import co.edu.udistrital.service.mapper.BandMemberMapper;
import co.edu.udistrital.web.rest.errors.ExceptionTranslator;
import co.edu.udistrital.service.dto.BandMemberCriteria;
import co.edu.udistrital.service.BandMemberQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static co.edu.udistrital.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link BandMemberResource} REST controller.
 */
@SpringBootTest(classes = MonoApp.class)
public class BandMemberResourceIT {

    @Autowired
    private BandMemberRepository bandMemberRepository;

    @Autowired
    private BandMemberMapper bandMemberMapper;

    @Autowired
    private BandMemberService bandMemberService;

    @Autowired
    private BandMemberQueryService bandMemberQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBandMemberMockMvc;

    private BandMember bandMember;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BandMemberResource bandMemberResource = new BandMemberResource(bandMemberService, bandMemberQueryService);
        this.restBandMemberMockMvc = MockMvcBuilders.standaloneSetup(bandMemberResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BandMember createEntity(EntityManager em) {
        BandMember bandMember = new BandMember();
        return bandMember;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BandMember createUpdatedEntity(EntityManager em) {
        BandMember bandMember = new BandMember();
        return bandMember;
    }

    @BeforeEach
    public void initTest() {
        bandMember = createEntity(em);
    }

    @Test
    @Transactional
    public void createBandMember() throws Exception {
        int databaseSizeBeforeCreate = bandMemberRepository.findAll().size();

        // Create the BandMember
        BandMemberDTO bandMemberDTO = bandMemberMapper.toDto(bandMember);
        restBandMemberMockMvc.perform(post("/api/band-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandMemberDTO)))
            .andExpect(status().isCreated());

        // Validate the BandMember in the database
        List<BandMember> bandMemberList = bandMemberRepository.findAll();
        assertThat(bandMemberList).hasSize(databaseSizeBeforeCreate + 1);
        BandMember testBandMember = bandMemberList.get(bandMemberList.size() - 1);
    }

    @Test
    @Transactional
    public void createBandMemberWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bandMemberRepository.findAll().size();

        // Create the BandMember with an existing ID
        bandMember.setId(1L);
        BandMemberDTO bandMemberDTO = bandMemberMapper.toDto(bandMember);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBandMemberMockMvc.perform(post("/api/band-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandMemberDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BandMember in the database
        List<BandMember> bandMemberList = bandMemberRepository.findAll();
        assertThat(bandMemberList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBandMembers() throws Exception {
        // Initialize the database
        bandMemberRepository.saveAndFlush(bandMember);

        // Get all the bandMemberList
        restBandMemberMockMvc.perform(get("/api/band-members?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bandMember.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getBandMember() throws Exception {
        // Initialize the database
        bandMemberRepository.saveAndFlush(bandMember);

        // Get the bandMember
        restBandMemberMockMvc.perform(get("/api/band-members/{id}", bandMember.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bandMember.getId().intValue()));
    }

    @Test
    @Transactional
    public void getAllBandMembersByBandusermembersIsEqualToSomething() throws Exception {
        // Initialize the database
        User bandusermembers = UserResourceIT.createEntity(em);
        em.persist(bandusermembers);
        em.flush();
        bandMember.setBandusermembers(bandusermembers);
        bandMemberRepository.saveAndFlush(bandMember);
        Long bandusermembersId = bandusermembers.getId();

        // Get all the bandMemberList where bandusermembers equals to bandusermembersId
        defaultBandMemberShouldBeFound("bandusermembersId.equals=" + bandusermembersId);

        // Get all the bandMemberList where bandusermembers equals to bandusermembersId + 1
        defaultBandMemberShouldNotBeFound("bandusermembersId.equals=" + (bandusermembersId + 1));
    }


    @Test
    @Transactional
    public void getAllBandMembersByBandMembersIsEqualToSomething() throws Exception {
        // Initialize the database
        Band bandMembers = BandResourceIT.createEntity(em);
        em.persist(bandMembers);
        em.flush();
        bandMember.setBandMembers(bandMembers);
        bandMemberRepository.saveAndFlush(bandMember);
        Long bandMembersId = bandMembers.getId();

        // Get all the bandMemberList where bandMembers equals to bandMembersId
        defaultBandMemberShouldBeFound("bandMembersId.equals=" + bandMembersId);

        // Get all the bandMemberList where bandMembers equals to bandMembersId + 1
        defaultBandMemberShouldNotBeFound("bandMembersId.equals=" + (bandMembersId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBandMemberShouldBeFound(String filter) throws Exception {
        restBandMemberMockMvc.perform(get("/api/band-members?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bandMember.getId().intValue())));

        // Check, that the count call also returns 1
        restBandMemberMockMvc.perform(get("/api/band-members/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBandMemberShouldNotBeFound(String filter) throws Exception {
        restBandMemberMockMvc.perform(get("/api/band-members?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBandMemberMockMvc.perform(get("/api/band-members/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBandMember() throws Exception {
        // Get the bandMember
        restBandMemberMockMvc.perform(get("/api/band-members/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBandMember() throws Exception {
        // Initialize the database
        bandMemberRepository.saveAndFlush(bandMember);

        int databaseSizeBeforeUpdate = bandMemberRepository.findAll().size();

        // Update the bandMember
        BandMember updatedBandMember = bandMemberRepository.findById(bandMember.getId()).get();
        // Disconnect from session so that the updates on updatedBandMember are not directly saved in db
        em.detach(updatedBandMember);
        BandMemberDTO bandMemberDTO = bandMemberMapper.toDto(updatedBandMember);

        restBandMemberMockMvc.perform(put("/api/band-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandMemberDTO)))
            .andExpect(status().isOk());

        // Validate the BandMember in the database
        List<BandMember> bandMemberList = bandMemberRepository.findAll();
        assertThat(bandMemberList).hasSize(databaseSizeBeforeUpdate);
        BandMember testBandMember = bandMemberList.get(bandMemberList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingBandMember() throws Exception {
        int databaseSizeBeforeUpdate = bandMemberRepository.findAll().size();

        // Create the BandMember
        BandMemberDTO bandMemberDTO = bandMemberMapper.toDto(bandMember);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBandMemberMockMvc.perform(put("/api/band-members")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bandMemberDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BandMember in the database
        List<BandMember> bandMemberList = bandMemberRepository.findAll();
        assertThat(bandMemberList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBandMember() throws Exception {
        // Initialize the database
        bandMemberRepository.saveAndFlush(bandMember);

        int databaseSizeBeforeDelete = bandMemberRepository.findAll().size();

        // Delete the bandMember
        restBandMemberMockMvc.perform(delete("/api/band-members/{id}", bandMember.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<BandMember> bandMemberList = bandMemberRepository.findAll();
        assertThat(bandMemberList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BandMember.class);
        BandMember bandMember1 = new BandMember();
        bandMember1.setId(1L);
        BandMember bandMember2 = new BandMember();
        bandMember2.setId(bandMember1.getId());
        assertThat(bandMember1).isEqualTo(bandMember2);
        bandMember2.setId(2L);
        assertThat(bandMember1).isNotEqualTo(bandMember2);
        bandMember1.setId(null);
        assertThat(bandMember1).isNotEqualTo(bandMember2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BandMemberDTO.class);
        BandMemberDTO bandMemberDTO1 = new BandMemberDTO();
        bandMemberDTO1.setId(1L);
        BandMemberDTO bandMemberDTO2 = new BandMemberDTO();
        assertThat(bandMemberDTO1).isNotEqualTo(bandMemberDTO2);
        bandMemberDTO2.setId(bandMemberDTO1.getId());
        assertThat(bandMemberDTO1).isEqualTo(bandMemberDTO2);
        bandMemberDTO2.setId(2L);
        assertThat(bandMemberDTO1).isNotEqualTo(bandMemberDTO2);
        bandMemberDTO1.setId(null);
        assertThat(bandMemberDTO1).isNotEqualTo(bandMemberDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bandMemberMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bandMemberMapper.fromId(null)).isNull();
    }
}
