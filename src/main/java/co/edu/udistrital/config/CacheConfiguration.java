package co.edu.udistrital.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, co.edu.udistrital.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, co.edu.udistrital.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, co.edu.udistrital.domain.User.class.getName());
            createCache(cm, co.edu.udistrital.domain.Authority.class.getName());
            createCache(cm, co.edu.udistrital.domain.User.class.getName() + ".authorities");
            createCache(cm, co.edu.udistrital.domain.Band.class.getName());
            createCache(cm, co.edu.udistrital.domain.Band.class.getName() + ".images");
            createCache(cm, co.edu.udistrital.domain.Band.class.getName() + ".categories");
            createCache(cm, co.edu.udistrital.domain.Band.class.getName() + ".members");
            createCache(cm, co.edu.udistrital.domain.Images.class.getName());
            createCache(cm, co.edu.udistrital.domain.Category.class.getName());
            createCache(cm, co.edu.udistrital.domain.Category.class.getName() + ".bandCategories");
            createCache(cm, co.edu.udistrital.domain.BandMember.class.getName());
            createCache(cm, co.edu.udistrital.domain.BandMember.class.getName() + ".bandBandMembers");
            createCache(cm, co.edu.udistrital.domain.BandMember.class.getName() + ".bandmembers");
            createCache(cm, co.edu.udistrital.domain.Band.class.getName() + ".memebers");
            createCache(cm, co.edu.udistrital.domain.Images.class.getName() + ".bandImages");
            createCache(cm, co.edu.udistrital.domain.Band.class.getName() + ".quotations");
            createCache(cm, co.edu.udistrital.domain.Quotation.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, jcacheConfiguration);
    }
}
