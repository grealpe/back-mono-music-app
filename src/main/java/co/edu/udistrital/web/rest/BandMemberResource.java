package co.edu.udistrital.web.rest;

import co.edu.udistrital.service.BandMemberService;
import co.edu.udistrital.web.rest.errors.BadRequestAlertException;
import co.edu.udistrital.service.dto.BandMemberDTO;
import co.edu.udistrital.service.dto.BandMemberCriteria;
import co.edu.udistrital.service.BandMemberQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link co.edu.udistrital.domain.BandMember}.
 */
@RestController
@RequestMapping("/api")
public class BandMemberResource {

    private final Logger log = LoggerFactory.getLogger(BandMemberResource.class);

    private static final String ENTITY_NAME = "bandMember";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BandMemberService bandMemberService;

    private final BandMemberQueryService bandMemberQueryService;

    public BandMemberResource(BandMemberService bandMemberService, BandMemberQueryService bandMemberQueryService) {
        this.bandMemberService = bandMemberService;
        this.bandMemberQueryService = bandMemberQueryService;
    }

    /**
     * {@code POST  /band-members} : Create a new bandMember.
     *
     * @param bandMemberDTO the bandMemberDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bandMemberDTO, or with status {@code 400 (Bad Request)} if the bandMember has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/band-members")
    public ResponseEntity<BandMemberDTO> createBandMember(@RequestBody BandMemberDTO bandMemberDTO) throws URISyntaxException {
        log.debug("REST request to save BandMember : {}", bandMemberDTO);
        if (bandMemberDTO.getId() != null) {
            throw new BadRequestAlertException("A new bandMember cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BandMemberDTO result = bandMemberService.save(bandMemberDTO);
        return ResponseEntity.created(new URI("/api/band-members/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /band-members} : Updates an existing bandMember.
     *
     * @param bandMemberDTO the bandMemberDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bandMemberDTO,
     * or with status {@code 400 (Bad Request)} if the bandMemberDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bandMemberDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/band-members")
    public ResponseEntity<BandMemberDTO> updateBandMember(@RequestBody BandMemberDTO bandMemberDTO) throws URISyntaxException {
        log.debug("REST request to update BandMember : {}", bandMemberDTO);
        if (bandMemberDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BandMemberDTO result = bandMemberService.save(bandMemberDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bandMemberDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /band-members} : get all the bandMembers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bandMembers in body.
     */
    @GetMapping("/band-members")
    public ResponseEntity<List<BandMemberDTO>> getAllBandMembers(BandMemberCriteria criteria) {
        log.debug("REST request to get BandMembers by criteria: {}", criteria);
        List<BandMemberDTO> entityList = bandMemberQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /band-members/count} : count all the bandMembers.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/band-members/count")
    public ResponseEntity<Long> countBandMembers(BandMemberCriteria criteria) {
        log.debug("REST request to count BandMembers by criteria: {}", criteria);
        return ResponseEntity.ok().body(bandMemberQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /band-members/:id} : get the "id" bandMember.
     *
     * @param id the id of the bandMemberDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bandMemberDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/band-members/{id}")
    public ResponseEntity<BandMemberDTO> getBandMember(@PathVariable Long id) {
        log.debug("REST request to get BandMember : {}", id);
        Optional<BandMemberDTO> bandMemberDTO = bandMemberService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bandMemberDTO);
    }

    /**
     * {@code DELETE  /band-members/:id} : delete the "id" bandMember.
     *
     * @param id the id of the bandMemberDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/band-members/{id}")
    public ResponseEntity<Void> deleteBandMember(@PathVariable Long id) {
        log.debug("REST request to delete BandMember : {}", id);
        bandMemberService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
