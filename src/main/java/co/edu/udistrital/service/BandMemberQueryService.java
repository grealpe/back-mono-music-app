package co.edu.udistrital.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import co.edu.udistrital.domain.BandMember;
import co.edu.udistrital.domain.*; // for static metamodels
import co.edu.udistrital.repository.BandMemberRepository;
import co.edu.udistrital.service.dto.BandMemberCriteria;
import co.edu.udistrital.service.dto.BandMemberDTO;
import co.edu.udistrital.service.mapper.BandMemberMapper;

/**
 * Service for executing complex queries for {@link BandMember} entities in the database.
 * The main input is a {@link BandMemberCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BandMemberDTO} or a {@link Page} of {@link BandMemberDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BandMemberQueryService extends QueryService<BandMember> {

    private final Logger log = LoggerFactory.getLogger(BandMemberQueryService.class);

    private final BandMemberRepository bandMemberRepository;

    private final BandMemberMapper bandMemberMapper;

    public BandMemberQueryService(BandMemberRepository bandMemberRepository, BandMemberMapper bandMemberMapper) {
        this.bandMemberRepository = bandMemberRepository;
        this.bandMemberMapper = bandMemberMapper;
    }

    /**
     * Return a {@link List} of {@link BandMemberDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BandMemberDTO> findByCriteria(BandMemberCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BandMember> specification = createSpecification(criteria);
        return bandMemberMapper.toDto(bandMemberRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BandMemberDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BandMemberDTO> findByCriteria(BandMemberCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BandMember> specification = createSpecification(criteria);
        return bandMemberRepository.findAll(specification, page)
            .map(bandMemberMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BandMemberCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BandMember> specification = createSpecification(criteria);
        return bandMemberRepository.count(specification);
    }

    /**
     * Function to convert BandMemberCriteria to a {@link Specification}.
     */
    private Specification<BandMember> createSpecification(BandMemberCriteria criteria) {
        Specification<BandMember> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), BandMember_.id));
            }
            if (criteria.getBandusermembersId() != null) {
                specification = specification.and(buildSpecification(criteria.getBandusermembersId(),
                    root -> root.join(BandMember_.bandusermembers, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getBandMembersId() != null) {
                specification = specification.and(buildSpecification(criteria.getBandMembersId(),
                    root -> root.join(BandMember_.bandMembers, JoinType.LEFT).get(Band_.id)));
            }
        }
        return specification;
    }
}
