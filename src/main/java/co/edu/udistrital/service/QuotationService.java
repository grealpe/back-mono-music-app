package co.edu.udistrital.service;

import co.edu.udistrital.service.dto.QuotationDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link co.edu.udistrital.domain.Quotation}.
 */
public interface QuotationService {

    /**
     * Save a quotation.
     *
     * @param quotationDTO the entity to save.
     * @return the persisted entity.
     */
    QuotationDTO save(QuotationDTO quotationDTO);

    /**
     * Get all the quotations.
     *
     * @return the list of entities.
     */
    List<QuotationDTO> findAll();


    /**
     * Get the "id" quotation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<QuotationDTO> findOne(Long id);

    /**
     * Delete the "id" quotation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
