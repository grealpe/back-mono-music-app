package co.edu.udistrital.service;

import co.edu.udistrital.service.dto.BandDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link co.edu.udistrital.domain.Band}.
 */
public interface BandService {

    /**
     * Save a band.
     *
     * @param bandDTO the entity to save.
     * @return the persisted entity.
     */
    BandDTO save(BandDTO bandDTO);

    /**
     * Get all the bands.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BandDTO> findAll(Pageable pageable);

    /**
     * Get all the bands with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<BandDTO> findAllWithEagerRelationships(Pageable pageable);
    
    /**
     * Get the "id" band.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BandDTO> findOne(Long id);

    /**
     * Delete the "id" band.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
