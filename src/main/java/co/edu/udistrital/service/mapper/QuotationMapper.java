package co.edu.udistrital.service.mapper;

import co.edu.udistrital.domain.*;
import co.edu.udistrital.service.dto.QuotationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Quotation} and its DTO {@link QuotationDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, BandMapper.class})
public interface QuotationMapper extends EntityMapper<QuotationDTO, Quotation> {

    @Mapping(source = "userQuotation.id", target = "userQuotationId")
    @Mapping(source = "userQuotation.firstName", target = "userQuotationFirstName")
    @Mapping(source = "bandQuotations.id", target = "bandQuotationsId")
    @Mapping(source = "bandQuotations.name", target = "bandQuotationsName")
    QuotationDTO toDto(Quotation quotation);

    @Mapping(source = "userQuotationId", target = "userQuotation")
    @Mapping(source = "bandQuotationsId", target = "bandQuotations")
    Quotation toEntity(QuotationDTO quotationDTO);

    default Quotation fromId(Long id) {
        if (id == null) {
            return null;
        }
        Quotation quotation = new Quotation();
        quotation.setId(id);
        return quotation;
    }
}
