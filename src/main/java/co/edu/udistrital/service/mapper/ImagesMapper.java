package co.edu.udistrital.service.mapper;

import co.edu.udistrital.domain.*;
import co.edu.udistrital.service.dto.ImagesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Images} and its DTO {@link ImagesDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ImagesMapper extends EntityMapper<ImagesDTO, Images> {


    @Mapping(target = "bandImages", ignore = true)
    Images toEntity(ImagesDTO imagesDTO);

    default Images fromId(Long id) {
        if (id == null) {
            return null;
        }
        Images images = new Images();
        images.setId(id);
        return images;
    }
}
