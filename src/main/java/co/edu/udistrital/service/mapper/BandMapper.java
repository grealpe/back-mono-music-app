package co.edu.udistrital.service.mapper;

import co.edu.udistrital.domain.*;
import co.edu.udistrital.service.dto.BandDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Band} and its DTO {@link BandDTO}.
 */
@Mapper(componentModel = "spring", uses = {CategoryMapper.class, ImagesMapper.class})
public interface BandMapper extends EntityMapper<BandDTO, Band> {


    @Mapping(target = "members", ignore = true)
    @Mapping(target = "quotations", ignore = true)
    Band toEntity(BandDTO bandDTO);

    default Band fromId(Long id) {
        if (id == null) {
            return null;
        }
        Band band = new Band();
        band.setId(id);
        return band;
    }
}
