package co.edu.udistrital.service.mapper;

import co.edu.udistrital.domain.*;
import co.edu.udistrital.service.dto.BandMemberDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BandMember} and its DTO {@link BandMemberDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, BandMapper.class})
public interface BandMemberMapper extends EntityMapper<BandMemberDTO, BandMember> {

    @Mapping(source = "bandusermembers.id", target = "bandusermembersId")
    @Mapping(source = "bandusermembers.firstName", target = "bandusermembersFirstName")
    @Mapping(source = "bandMembers.id", target = "bandMembersId")
    @Mapping(source = "bandMembers.name", target = "bandMembersName")
    BandMemberDTO toDto(BandMember bandMember);

    @Mapping(source = "bandusermembersId", target = "bandusermembers")
    @Mapping(source = "bandMembersId", target = "bandMembers")
    BandMember toEntity(BandMemberDTO bandMemberDTO);

    default BandMember fromId(Long id) {
        if (id == null) {
            return null;
        }
        BandMember bandMember = new BandMember();
        bandMember.setId(id);
        return bandMember;
    }
}
