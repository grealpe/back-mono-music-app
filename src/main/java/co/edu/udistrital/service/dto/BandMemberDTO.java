package co.edu.udistrital.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link co.edu.udistrital.domain.BandMember} entity.
 */
public class BandMemberDTO implements Serializable {

    private Long id;


    private Long bandusermembersId;

    private String bandusermembersFirstName;

    private Long bandMembersId;

    private String bandMembersName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBandusermembersId() {
        return bandusermembersId;
    }

    public void setBandusermembersId(Long userId) {
        this.bandusermembersId = userId;
    }

    public String getBandusermembersFirstName() {
        return bandusermembersFirstName;
    }

    public void setBandusermembersFirstName(String userFirstName) {
        this.bandusermembersFirstName = userFirstName;
    }

    public Long getBandMembersId() {
        return bandMembersId;
    }

    public void setBandMembersId(Long bandId) {
        this.bandMembersId = bandId;
    }

    public String getBandMembersName() {
        return bandMembersName;
    }

    public void setBandMembersName(String bandName) {
        this.bandMembersName = bandName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BandMemberDTO bandMemberDTO = (BandMemberDTO) o;
        if (bandMemberDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bandMemberDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BandMemberDTO{" +
            "id=" + getId() +
            ", bandusermembers=" + getBandusermembersId() +
            ", bandusermembers='" + getBandusermembersFirstName() + "'" +
            ", bandMembers=" + getBandMembersId() +
            ", bandMembers='" + getBandMembersName() + "'" +
            "}";
    }
}
