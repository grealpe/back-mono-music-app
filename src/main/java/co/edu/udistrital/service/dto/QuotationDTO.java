package co.edu.udistrital.service.dto;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link co.edu.udistrital.domain.Quotation} entity.
 */
public class QuotationDTO implements Serializable {

    private Long id;

    private ZonedDateTime date;

    private String name;

    private String lastName;

    private String address;

    private String phone;

    private String email;

    @Size(max = 4096)
    private String observation;


    private Long userQuotationId;

    private String userQuotationFirstName;

    private Long bandQuotationsId;

    private String bandQuotationsName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Long getUserQuotationId() {
        return userQuotationId;
    }

    public void setUserQuotationId(Long userId) {
        this.userQuotationId = userId;
    }

    public String getUserQuotationFirstName() {
        return userQuotationFirstName;
    }

    public void setUserQuotationFirstName(String userFirstName) {
        this.userQuotationFirstName = userFirstName;
    }

    public Long getBandQuotationsId() {
        return bandQuotationsId;
    }

    public void setBandQuotationsId(Long bandId) {
        this.bandQuotationsId = bandId;
    }

    public String getBandQuotationsName() {
        return bandQuotationsName;
    }

    public void setBandQuotationsName(String bandName) {
        this.bandQuotationsName = bandName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QuotationDTO quotationDTO = (QuotationDTO) o;
        if (quotationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), quotationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "QuotationDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", name='" + getName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", address='" + getAddress() + "'" +
            ", phone='" + getPhone() + "'" +
            ", email='" + getEmail() + "'" +
            ", observation='" + getObservation() + "'" +
            ", userQuotation=" + getUserQuotationId() +
            ", userQuotation='" + getUserQuotationFirstName() + "'" +
            ", bandQuotations=" + getBandQuotationsId() +
            ", bandQuotations='" + getBandQuotationsName() + "'" +
            "}";
    }
}
