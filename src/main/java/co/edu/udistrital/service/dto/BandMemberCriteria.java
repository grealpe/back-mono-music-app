package co.edu.udistrital.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link co.edu.udistrital.domain.BandMember} entity. This class is used
 * in {@link co.edu.udistrital.web.rest.BandMemberResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /band-members?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BandMemberCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter bandusermembersId;

    private LongFilter bandMembersId;

    public BandMemberCriteria(){
    }

    public BandMemberCriteria(BandMemberCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.bandusermembersId = other.bandusermembersId == null ? null : other.bandusermembersId.copy();
        this.bandMembersId = other.bandMembersId == null ? null : other.bandMembersId.copy();
    }

    @Override
    public BandMemberCriteria copy() {
        return new BandMemberCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getBandusermembersId() {
        return bandusermembersId;
    }

    public void setBandusermembersId(LongFilter bandusermembersId) {
        this.bandusermembersId = bandusermembersId;
    }

    public LongFilter getBandMembersId() {
        return bandMembersId;
    }

    public void setBandMembersId(LongFilter bandMembersId) {
        this.bandMembersId = bandMembersId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BandMemberCriteria that = (BandMemberCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(bandusermembersId, that.bandusermembersId) &&
            Objects.equals(bandMembersId, that.bandMembersId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        bandusermembersId,
        bandMembersId
        );
    }

    @Override
    public String toString() {
        return "BandMemberCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (bandusermembersId != null ? "bandusermembersId=" + bandusermembersId + ", " : "") +
                (bandMembersId != null ? "bandMembersId=" + bandMembersId + ", " : "") +
            "}";
    }

}
