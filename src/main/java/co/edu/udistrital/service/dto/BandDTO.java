package co.edu.udistrital.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the {@link co.edu.udistrital.domain.Band} entity.
 */
public class BandDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String video;

    @NotNull
    @Size(max = 1024)
    private String description;

    @DecimalMin(value = "0")
    @DecimalMax(value = "5")
    private Double star;


    private Set<CategoryDTO> categories = new HashSet<>();

    private Set<ImagesDTO> images = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getStar() {
        return star;
    }

    public void setStar(Double star) {
        this.star = star;
    }

    public Set<CategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(Set<CategoryDTO> categories) {
        this.categories = categories;
    }

    public Set<ImagesDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImagesDTO> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BandDTO bandDTO = (BandDTO) o;
        if (bandDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bandDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BandDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", video='" + getVideo() + "'" +
            ", description='" + getDescription() + "'" +
            ", star=" + getStar() +
            "}";
    }
}
