package co.edu.udistrital.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link co.edu.udistrital.domain.Images} entity.
 */
public class ImagesDTO implements Serializable {

    private Long id;

    @NotNull
    private String url;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ImagesDTO imagesDTO = (ImagesDTO) o;
        if (imagesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), imagesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ImagesDTO{" +
            "id=" + getId() +
            ", url='" + getUrl() + "'" +
            "}";
    }
}
