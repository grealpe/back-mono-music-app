package co.edu.udistrital.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link co.edu.udistrital.domain.Band} entity. This class is used
 * in {@link co.edu.udistrital.web.rest.BandResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bands?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BandCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter video;

    private StringFilter description;

    private DoubleFilter star;

    private LongFilter membersId;

    private LongFilter quotationsId;

    private LongFilter categoriesId;

    private LongFilter imagesId;

    public BandCriteria(){
    }

    public BandCriteria(BandCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.video = other.video == null ? null : other.video.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.star = other.star == null ? null : other.star.copy();
        this.membersId = other.membersId == null ? null : other.membersId.copy();
        this.quotationsId = other.quotationsId == null ? null : other.quotationsId.copy();
        this.categoriesId = other.categoriesId == null ? null : other.categoriesId.copy();
        this.imagesId = other.imagesId == null ? null : other.imagesId.copy();
    }

    @Override
    public BandCriteria copy() {
        return new BandCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getVideo() {
        return video;
    }

    public void setVideo(StringFilter video) {
        this.video = video;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public DoubleFilter getStar() {
        return star;
    }

    public void setStar(DoubleFilter star) {
        this.star = star;
    }

    public LongFilter getMembersId() {
        return membersId;
    }

    public void setMembersId(LongFilter membersId) {
        this.membersId = membersId;
    }

    public LongFilter getQuotationsId() {
        return quotationsId;
    }

    public void setQuotationsId(LongFilter quotationsId) {
        this.quotationsId = quotationsId;
    }

    public LongFilter getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(LongFilter categoriesId) {
        this.categoriesId = categoriesId;
    }

    public LongFilter getImagesId() {
        return imagesId;
    }

    public void setImagesId(LongFilter imagesId) {
        this.imagesId = imagesId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BandCriteria that = (BandCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(video, that.video) &&
            Objects.equals(description, that.description) &&
            Objects.equals(star, that.star) &&
            Objects.equals(membersId, that.membersId) &&
            Objects.equals(quotationsId, that.quotationsId) &&
            Objects.equals(categoriesId, that.categoriesId) &&
            Objects.equals(imagesId, that.imagesId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        video,
        description,
        star,
        membersId,
        quotationsId,
        categoriesId,
        imagesId
        );
    }

    @Override
    public String toString() {
        return "BandCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (video != null ? "video=" + video + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (star != null ? "star=" + star + ", " : "") +
                (membersId != null ? "membersId=" + membersId + ", " : "") +
                (quotationsId != null ? "quotationsId=" + quotationsId + ", " : "") +
                (categoriesId != null ? "categoriesId=" + categoriesId + ", " : "") +
                (imagesId != null ? "imagesId=" + imagesId + ", " : "") +
            "}";
    }

}
