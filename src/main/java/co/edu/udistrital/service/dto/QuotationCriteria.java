package co.edu.udistrital.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.ZonedDateTimeFilter;

/**
 * Criteria class for the {@link co.edu.udistrital.domain.Quotation} entity. This class is used
 * in {@link co.edu.udistrital.web.rest.QuotationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /quotations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class QuotationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private ZonedDateTimeFilter date;

    private StringFilter name;

    private StringFilter lastName;

    private StringFilter address;

    private StringFilter phone;

    private StringFilter email;

    private StringFilter observation;

    private LongFilter userQuotationId;

    private LongFilter bandQuotationsId;

    public QuotationCriteria(){
    }

    public QuotationCriteria(QuotationCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.date = other.date == null ? null : other.date.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.lastName = other.lastName == null ? null : other.lastName.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.observation = other.observation == null ? null : other.observation.copy();
        this.userQuotationId = other.userQuotationId == null ? null : other.userQuotationId.copy();
        this.bandQuotationsId = other.bandQuotationsId == null ? null : other.bandQuotationsId.copy();
    }

    @Override
    public QuotationCriteria copy() {
        return new QuotationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public ZonedDateTimeFilter getDate() {
        return date;
    }

    public void setDate(ZonedDateTimeFilter date) {
        this.date = date;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getLastName() {
        return lastName;
    }

    public void setLastName(StringFilter lastName) {
        this.lastName = lastName;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getObservation() {
        return observation;
    }

    public void setObservation(StringFilter observation) {
        this.observation = observation;
    }

    public LongFilter getUserQuotationId() {
        return userQuotationId;
    }

    public void setUserQuotationId(LongFilter userQuotationId) {
        this.userQuotationId = userQuotationId;
    }

    public LongFilter getBandQuotationsId() {
        return bandQuotationsId;
    }

    public void setBandQuotationsId(LongFilter bandQuotationsId) {
        this.bandQuotationsId = bandQuotationsId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final QuotationCriteria that = (QuotationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(date, that.date) &&
            Objects.equals(name, that.name) &&
            Objects.equals(lastName, that.lastName) &&
            Objects.equals(address, that.address) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(email, that.email) &&
            Objects.equals(observation, that.observation) &&
            Objects.equals(userQuotationId, that.userQuotationId) &&
            Objects.equals(bandQuotationsId, that.bandQuotationsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        date,
        name,
        lastName,
        address,
        phone,
        email,
        observation,
        userQuotationId,
        bandQuotationsId
        );
    }

    @Override
    public String toString() {
        return "QuotationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (date != null ? "date=" + date + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (lastName != null ? "lastName=" + lastName + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (observation != null ? "observation=" + observation + ", " : "") +
                (userQuotationId != null ? "userQuotationId=" + userQuotationId + ", " : "") +
                (bandQuotationsId != null ? "bandQuotationsId=" + bandQuotationsId + ", " : "") +
            "}";
    }

}
