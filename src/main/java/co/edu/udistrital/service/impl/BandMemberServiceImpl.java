package co.edu.udistrital.service.impl;

import co.edu.udistrital.service.BandMemberService;
import co.edu.udistrital.domain.BandMember;
import co.edu.udistrital.repository.BandMemberRepository;
import co.edu.udistrital.service.dto.BandMemberDTO;
import co.edu.udistrital.service.mapper.BandMemberMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link BandMember}.
 */
@Service
@Transactional
public class BandMemberServiceImpl implements BandMemberService {

    private final Logger log = LoggerFactory.getLogger(BandMemberServiceImpl.class);

    private final BandMemberRepository bandMemberRepository;

    private final BandMemberMapper bandMemberMapper;

    public BandMemberServiceImpl(BandMemberRepository bandMemberRepository, BandMemberMapper bandMemberMapper) {
        this.bandMemberRepository = bandMemberRepository;
        this.bandMemberMapper = bandMemberMapper;
    }

    /**
     * Save a bandMember.
     *
     * @param bandMemberDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BandMemberDTO save(BandMemberDTO bandMemberDTO) {
        log.debug("Request to save BandMember : {}", bandMemberDTO);
        BandMember bandMember = bandMemberMapper.toEntity(bandMemberDTO);
        bandMember = bandMemberRepository.save(bandMember);
        return bandMemberMapper.toDto(bandMember);
    }

    /**
     * Get all the bandMembers.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<BandMemberDTO> findAll() {
        log.debug("Request to get all BandMembers");
        return bandMemberRepository.findAll().stream()
            .map(bandMemberMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one bandMember by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BandMemberDTO> findOne(Long id) {
        log.debug("Request to get BandMember : {}", id);
        return bandMemberRepository.findById(id)
            .map(bandMemberMapper::toDto);
    }

    /**
     * Delete the bandMember by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BandMember : {}", id);
        bandMemberRepository.deleteById(id);
    }
}
