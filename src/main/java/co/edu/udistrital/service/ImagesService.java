package co.edu.udistrital.service;

import co.edu.udistrital.service.dto.ImagesDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link co.edu.udistrital.domain.Images}.
 */
public interface ImagesService {

    /**
     * Save a images.
     *
     * @param imagesDTO the entity to save.
     * @return the persisted entity.
     */
    ImagesDTO save(ImagesDTO imagesDTO);

    /**
     * Get all the images.
     *
     * @return the list of entities.
     */
    List<ImagesDTO> findAll();


    /**
     * Get the "id" images.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ImagesDTO> findOne(Long id);

    /**
     * Delete the "id" images.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
