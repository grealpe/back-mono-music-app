package co.edu.udistrital.service;

import co.edu.udistrital.service.dto.BandMemberDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link co.edu.udistrital.domain.BandMember}.
 */
public interface BandMemberService {

    /**
     * Save a bandMember.
     *
     * @param bandMemberDTO the entity to save.
     * @return the persisted entity.
     */
    BandMemberDTO save(BandMemberDTO bandMemberDTO);

    /**
     * Get all the bandMembers.
     *
     * @return the list of entities.
     */
    List<BandMemberDTO> findAll();


    /**
     * Get the "id" bandMember.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BandMemberDTO> findOne(Long id);

    /**
     * Delete the "id" bandMember.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
