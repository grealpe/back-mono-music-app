package co.edu.udistrital.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A BandMember.
 */
@Entity
@Table(name = "band_member")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BandMember implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("bandMembers")
    private User bandusermembers;

    @ManyToOne
    @JsonIgnoreProperties("members")
    private Band bandMembers;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getBandusermembers() {
        return bandusermembers;
    }

    public BandMember bandusermembers(User user) {
        this.bandusermembers = user;
        return this;
    }

    public void setBandusermembers(User user) {
        this.bandusermembers = user;
    }

    public Band getBandMembers() {
        return bandMembers;
    }

    public BandMember bandMembers(Band band) {
        this.bandMembers = band;
        return this;
    }

    public void setBandMembers(Band band) {
        this.bandMembers = band;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BandMember)) {
            return false;
        }
        return id != null && id.equals(((BandMember) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BandMember{" +
            "id=" + getId() +
            "}";
    }
}
