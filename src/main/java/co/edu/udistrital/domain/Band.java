package co.edu.udistrital.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Band.
 */
@Entity
@Table(name = "band")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Band implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "video")
    private String video;

    @NotNull
    @Size(max = 1024)
    @Column(name = "description", length = 1024, nullable = false)
    private String description;

    @DecimalMin(value = "0")
    @DecimalMax(value = "5")
    @Column(name = "star")
    private Double star;

    @OneToMany(mappedBy = "bandMembers")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BandMember> members = new HashSet<>();

    @OneToMany(mappedBy = "bandQuotations")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Quotation> quotations = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "band_categories",
               joinColumns = @JoinColumn(name = "band_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "categories_id", referencedColumnName = "id"))
    private Set<Category> categories = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "band_images",
               joinColumns = @JoinColumn(name = "band_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "images_id", referencedColumnName = "id"))
    private Set<Images> images = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Band name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideo() {
        return video;
    }

    public Band video(String video) {
        this.video = video;
        return this;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getDescription() {
        return description;
    }

    public Band description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getStar() {
        return star;
    }

    public Band star(Double star) {
        this.star = star;
        return this;
    }

    public void setStar(Double star) {
        this.star = star;
    }

    public Set<BandMember> getMembers() {
        return members;
    }

    public Band members(Set<BandMember> bandMembers) {
        this.members = bandMembers;
        return this;
    }

    public Band addMembers(BandMember bandMember) {
        this.members.add(bandMember);
        bandMember.setBandMembers(this);
        return this;
    }

    public Band removeMembers(BandMember bandMember) {
        this.members.remove(bandMember);
        bandMember.setBandMembers(null);
        return this;
    }

    public void setMembers(Set<BandMember> bandMembers) {
        this.members = bandMembers;
    }

    public Set<Quotation> getQuotations() {
        return quotations;
    }

    public Band quotations(Set<Quotation> quotations) {
        this.quotations = quotations;
        return this;
    }

    public Band addQuotations(Quotation quotation) {
        this.quotations.add(quotation);
        quotation.setBandQuotations(this);
        return this;
    }

    public Band removeQuotations(Quotation quotation) {
        this.quotations.remove(quotation);
        quotation.setBandQuotations(null);
        return this;
    }

    public void setQuotations(Set<Quotation> quotations) {
        this.quotations = quotations;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public Band categories(Set<Category> categories) {
        this.categories = categories;
        return this;
    }

    public Band addCategories(Category category) {
        this.categories.add(category);
        category.getBandCategories().add(this);
        return this;
    }

    public Band removeCategories(Category category) {
        this.categories.remove(category);
        category.getBandCategories().remove(this);
        return this;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public Set<Images> getImages() {
        return images;
    }

    public Band images(Set<Images> images) {
        this.images = images;
        return this;
    }

    public Band addImages(Images images) {
        this.images.add(images);
        images.getBandImages().add(this);
        return this;
    }

    public Band removeImages(Images images) {
        this.images.remove(images);
        images.getBandImages().remove(this);
        return this;
    }

    public void setImages(Set<Images> images) {
        this.images = images;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Band)) {
            return false;
        }
        return id != null && id.equals(((Band) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Band{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", video='" + getVideo() + "'" +
            ", description='" + getDescription() + "'" +
            ", star=" + getStar() +
            "}";
    }
}
