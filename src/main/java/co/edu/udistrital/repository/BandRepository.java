package co.edu.udistrital.repository;

import co.edu.udistrital.domain.Band;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Band entity.
 */
@Repository
public interface BandRepository extends JpaRepository<Band, Long>, JpaSpecificationExecutor<Band> {

    @Query(value = "select distinct band from Band band left join fetch band.categories left join fetch band.images",
        countQuery = "select count(distinct band) from Band band")
    Page<Band> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct band from Band band left join fetch band.categories left join fetch band.images")
    List<Band> findAllWithEagerRelationships();

    @Query("select band from Band band left join fetch band.categories left join fetch band.images where band.id =:id")
    Optional<Band> findOneWithEagerRelationships(@Param("id") Long id);

}
