package co.edu.udistrital.repository;

import co.edu.udistrital.domain.Quotation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Quotation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuotationRepository extends JpaRepository<Quotation, Long>, JpaSpecificationExecutor<Quotation> {

    @Query("select quotation from Quotation quotation where quotation.userQuotation.login = ?#{principal.username}")
    List<Quotation> findByUserQuotationIsCurrentUser();

}
