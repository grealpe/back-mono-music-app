package co.edu.udistrital.repository;

import co.edu.udistrital.domain.BandMember;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the BandMember entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BandMemberRepository extends JpaRepository<BandMember, Long>, JpaSpecificationExecutor<BandMember> {

    @Query("select bandMember from BandMember bandMember where bandMember.bandusermembers.login = ?#{principal.username}")
    List<BandMember> findByBandusermembersIsCurrentUser();

}
