import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IBand, Band } from 'app/shared/model/band.model';
import { BandService } from './band.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category';
import { IImages } from 'app/shared/model/images.model';
import { ImagesService } from 'app/entities/images';

@Component({
  selector: 'jhi-band-update',
  templateUrl: './band-update.component.html'
})
export class BandUpdateComponent implements OnInit {
  band: IBand;
  isSaving: boolean;

  categories: ICategory[];

  images: IImages[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    video: [],
    description: [null, [Validators.required, Validators.maxLength(1024)]],
    star: [null, [Validators.min(0), Validators.max(5)]],
    categories: [],
    images: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected bandService: BandService,
    protected categoryService: CategoryService,
    protected imagesService: ImagesService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ band }) => {
      this.updateForm(band);
      this.band = band;
    });
    this.categoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICategory[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICategory[]>) => response.body)
      )
      .subscribe((res: ICategory[]) => (this.categories = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.imagesService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IImages[]>) => mayBeOk.ok),
        map((response: HttpResponse<IImages[]>) => response.body)
      )
      .subscribe((res: IImages[]) => (this.images = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(band: IBand) {
    this.editForm.patchValue({
      id: band.id,
      name: band.name,
      video: band.video,
      description: band.description,
      star: band.star,
      categories: band.categories,
      images: band.images
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const band = this.createFromForm();
    if (band.id !== undefined) {
      this.subscribeToSaveResponse(this.bandService.update(band));
    } else {
      this.subscribeToSaveResponse(this.bandService.create(band));
    }
  }

  private createFromForm(): IBand {
    const entity = {
      ...new Band(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      video: this.editForm.get(['video']).value,
      description: this.editForm.get(['description']).value,
      star: this.editForm.get(['star']).value,
      categories: this.editForm.get(['categories']).value,
      images: this.editForm.get(['images']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBand>>) {
    result.subscribe((res: HttpResponse<IBand>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCategoryById(index: number, item: ICategory) {
    return item.id;
  }

  trackImagesById(index: number, item: IImages) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
