import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'band',
        loadChildren: './band/band.module#MonoBandModule'
      },
      {
        path: 'images',
        loadChildren: './images/images.module#MonoImagesModule'
      },
      {
        path: 'category',
        loadChildren: './category/category.module#MonoCategoryModule'
      },
      {
        path: 'band-member',
        loadChildren: './band-member/band-member.module#MonoBandMemberModule'
      },
      {
        path: 'band',
        loadChildren: './band/band.module#MonoBandModule'
      },
      {
        path: 'band-member',
        loadChildren: './band-member/band-member.module#MonoBandMemberModule'
      },
      {
        path: 'band',
        loadChildren: './band/band.module#MonoBandModule'
      },
      {
        path: 'band-member',
        loadChildren: './band-member/band-member.module#MonoBandMemberModule'
      },
      {
        path: 'band',
        loadChildren: './band/band.module#MonoBandModule'
      },
      {
        path: 'band-member',
        loadChildren: './band-member/band-member.module#MonoBandMemberModule'
      },
      {
        path: 'band',
        loadChildren: './band/band.module#MonoBandModule'
      },
      {
        path: 'images',
        loadChildren: './images/images.module#MonoImagesModule'
      },
      {
        path: 'band',
        loadChildren: './band/band.module#MonoBandModule'
      },
      {
        path: 'band',
        loadChildren: './band/band.module#MonoBandModule'
      },
      {
        path: 'quotation',
        loadChildren: './quotation/quotation.module#MonoQuotationModule'
      },
      {
        path: 'quotation',
        loadChildren: './quotation/quotation.module#MonoQuotationModule'
      },
      {
        path: 'quotation',
        loadChildren: './quotation/quotation.module#MonoQuotationModule'
      },
      {
        path: 'band-member',
        loadChildren: './band-member/band-member.module#MonoBandMemberModule'
      },
      {
        path: 'band',
        loadChildren: './band/band.module#MonoBandModule'
      },
      {
        path: 'category',
        loadChildren: './category/category.module#MonoCategoryModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MonoEntityModule {}
