import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Images } from 'app/shared/model/images.model';
import { ImagesService } from './images.service';
import { ImagesComponent } from './images.component';
import { ImagesDetailComponent } from './images-detail.component';
import { ImagesUpdateComponent } from './images-update.component';
import { ImagesDeletePopupComponent } from './images-delete-dialog.component';
import { IImages } from 'app/shared/model/images.model';

@Injectable({ providedIn: 'root' })
export class ImagesResolve implements Resolve<IImages> {
  constructor(private service: ImagesService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IImages> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Images>) => response.ok),
        map((images: HttpResponse<Images>) => images.body)
      );
    }
    return of(new Images());
  }
}

export const imagesRoute: Routes = [
  {
    path: '',
    component: ImagesComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.images.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ImagesDetailComponent,
    resolve: {
      images: ImagesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.images.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ImagesUpdateComponent,
    resolve: {
      images: ImagesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.images.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ImagesUpdateComponent,
    resolve: {
      images: ImagesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.images.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const imagesPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ImagesDeletePopupComponent,
    resolve: {
      images: ImagesResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.images.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
