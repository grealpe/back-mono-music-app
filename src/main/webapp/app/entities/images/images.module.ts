import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MonoSharedModule } from 'app/shared';
import {
  ImagesComponent,
  ImagesDetailComponent,
  ImagesUpdateComponent,
  ImagesDeletePopupComponent,
  ImagesDeleteDialogComponent,
  imagesRoute,
  imagesPopupRoute
} from './';

const ENTITY_STATES = [...imagesRoute, ...imagesPopupRoute];

@NgModule({
  imports: [MonoSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [ImagesComponent, ImagesDetailComponent, ImagesUpdateComponent, ImagesDeleteDialogComponent, ImagesDeletePopupComponent],
  entryComponents: [ImagesComponent, ImagesUpdateComponent, ImagesDeleteDialogComponent, ImagesDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MonoImagesModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
