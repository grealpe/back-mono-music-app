import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IImages, Images } from 'app/shared/model/images.model';
import { ImagesService } from './images.service';
import { IBand } from 'app/shared/model/band.model';
import { BandService } from 'app/entities/band';

@Component({
  selector: 'jhi-images-update',
  templateUrl: './images-update.component.html'
})
export class ImagesUpdateComponent implements OnInit {
  images: IImages;
  isSaving: boolean;

  bands: IBand[];

  editForm = this.fb.group({
    id: [],
    url: [null, [Validators.required]]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected imagesService: ImagesService,
    protected bandService: BandService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ images }) => {
      this.updateForm(images);
      this.images = images;
    });
    this.bandService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBand[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBand[]>) => response.body)
      )
      .subscribe((res: IBand[]) => (this.bands = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(images: IImages) {
    this.editForm.patchValue({
      id: images.id,
      url: images.url
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const images = this.createFromForm();
    if (images.id !== undefined) {
      this.subscribeToSaveResponse(this.imagesService.update(images));
    } else {
      this.subscribeToSaveResponse(this.imagesService.create(images));
    }
  }

  private createFromForm(): IImages {
    const entity = {
      ...new Images(),
      id: this.editForm.get(['id']).value,
      url: this.editForm.get(['url']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImages>>) {
    result.subscribe((res: HttpResponse<IImages>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBandById(index: number, item: IBand) {
    return item.id;
  }

  getSelected(selectedVals: Array<any>, option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
