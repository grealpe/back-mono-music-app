import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IImages } from 'app/shared/model/images.model';
import { ImagesService } from './images.service';

@Component({
  selector: 'jhi-images-delete-dialog',
  templateUrl: './images-delete-dialog.component.html'
})
export class ImagesDeleteDialogComponent {
  images: IImages;

  constructor(protected imagesService: ImagesService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.imagesService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'imagesListModification',
        content: 'Deleted an images'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-images-delete-popup',
  template: ''
})
export class ImagesDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ images }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ImagesDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.images = images;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/images', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/images', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
