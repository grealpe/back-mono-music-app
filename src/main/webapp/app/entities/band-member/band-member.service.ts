import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBandMember } from 'app/shared/model/band-member.model';

type EntityResponseType = HttpResponse<IBandMember>;
type EntityArrayResponseType = HttpResponse<IBandMember[]>;

@Injectable({ providedIn: 'root' })
export class BandMemberService {
  public resourceUrl = SERVER_API_URL + 'api/band-members';

  constructor(protected http: HttpClient) {}

  create(bandMember: IBandMember): Observable<EntityResponseType> {
    return this.http.post<IBandMember>(this.resourceUrl, bandMember, { observe: 'response' });
  }

  update(bandMember: IBandMember): Observable<EntityResponseType> {
    return this.http.put<IBandMember>(this.resourceUrl, bandMember, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBandMember>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBandMember[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
