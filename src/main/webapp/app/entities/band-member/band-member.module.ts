import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MonoSharedModule } from 'app/shared';
import {
  BandMemberComponent,
  BandMemberDetailComponent,
  BandMemberUpdateComponent,
  BandMemberDeletePopupComponent,
  BandMemberDeleteDialogComponent,
  bandMemberRoute,
  bandMemberPopupRoute
} from './';

const ENTITY_STATES = [...bandMemberRoute, ...bandMemberPopupRoute];

@NgModule({
  imports: [MonoSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BandMemberComponent,
    BandMemberDetailComponent,
    BandMemberUpdateComponent,
    BandMemberDeleteDialogComponent,
    BandMemberDeletePopupComponent
  ],
  entryComponents: [BandMemberComponent, BandMemberUpdateComponent, BandMemberDeleteDialogComponent, BandMemberDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MonoBandMemberModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
