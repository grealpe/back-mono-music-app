import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBandMember } from 'app/shared/model/band-member.model';

@Component({
  selector: 'jhi-band-member-detail',
  templateUrl: './band-member-detail.component.html'
})
export class BandMemberDetailComponent implements OnInit {
  bandMember: IBandMember;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bandMember }) => {
      this.bandMember = bandMember;
    });
  }

  previousState() {
    window.history.back();
  }
}
