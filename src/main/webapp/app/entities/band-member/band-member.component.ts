import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IBandMember } from 'app/shared/model/band-member.model';
import { AccountService } from 'app/core';
import { BandMemberService } from './band-member.service';

@Component({
  selector: 'jhi-band-member',
  templateUrl: './band-member.component.html'
})
export class BandMemberComponent implements OnInit, OnDestroy {
  bandMembers: IBandMember[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected bandMemberService: BandMemberService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.bandMemberService
      .query()
      .pipe(
        filter((res: HttpResponse<IBandMember[]>) => res.ok),
        map((res: HttpResponse<IBandMember[]>) => res.body)
      )
      .subscribe(
        (res: IBandMember[]) => {
          this.bandMembers = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInBandMembers();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IBandMember) {
    return item.id;
  }

  registerChangeInBandMembers() {
    this.eventSubscriber = this.eventManager.subscribe('bandMemberListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
