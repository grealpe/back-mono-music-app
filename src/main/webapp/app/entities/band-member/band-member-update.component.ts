import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IBandMember, BandMember } from 'app/shared/model/band-member.model';
import { BandMemberService } from './band-member.service';
import { IUser, UserService } from 'app/core';
import { IBand } from 'app/shared/model/band.model';
import { BandService } from 'app/entities/band';

@Component({
  selector: 'jhi-band-member-update',
  templateUrl: './band-member-update.component.html'
})
export class BandMemberUpdateComponent implements OnInit {
  bandMember: IBandMember;
  isSaving: boolean;

  users: IUser[];

  bands: IBand[];

  editForm = this.fb.group({
    id: [],
    bandusermembersId: [],
    bandMembersId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected bandMemberService: BandMemberService,
    protected userService: UserService,
    protected bandService: BandService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ bandMember }) => {
      this.updateForm(bandMember);
      this.bandMember = bandMember;
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bandService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBand[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBand[]>) => response.body)
      )
      .subscribe((res: IBand[]) => (this.bands = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(bandMember: IBandMember) {
    this.editForm.patchValue({
      id: bandMember.id,
      bandusermembersId: bandMember.bandusermembersId,
      bandMembersId: bandMember.bandMembersId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const bandMember = this.createFromForm();
    if (bandMember.id !== undefined) {
      this.subscribeToSaveResponse(this.bandMemberService.update(bandMember));
    } else {
      this.subscribeToSaveResponse(this.bandMemberService.create(bandMember));
    }
  }

  private createFromForm(): IBandMember {
    const entity = {
      ...new BandMember(),
      id: this.editForm.get(['id']).value,
      bandusermembersId: this.editForm.get(['bandusermembersId']).value,
      bandMembersId: this.editForm.get(['bandMembersId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBandMember>>) {
    result.subscribe((res: HttpResponse<IBandMember>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackBandById(index: number, item: IBand) {
    return item.id;
  }
}
