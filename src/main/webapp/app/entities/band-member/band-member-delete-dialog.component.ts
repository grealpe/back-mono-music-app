import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBandMember } from 'app/shared/model/band-member.model';
import { BandMemberService } from './band-member.service';

@Component({
  selector: 'jhi-band-member-delete-dialog',
  templateUrl: './band-member-delete-dialog.component.html'
})
export class BandMemberDeleteDialogComponent {
  bandMember: IBandMember;

  constructor(
    protected bandMemberService: BandMemberService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.bandMemberService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'bandMemberListModification',
        content: 'Deleted an bandMember'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-band-member-delete-popup',
  template: ''
})
export class BandMemberDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ bandMember }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BandMemberDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.bandMember = bandMember;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/band-member', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/band-member', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
