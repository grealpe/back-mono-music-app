import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BandMember } from 'app/shared/model/band-member.model';
import { BandMemberService } from './band-member.service';
import { BandMemberComponent } from './band-member.component';
import { BandMemberDetailComponent } from './band-member-detail.component';
import { BandMemberUpdateComponent } from './band-member-update.component';
import { BandMemberDeletePopupComponent } from './band-member-delete-dialog.component';
import { IBandMember } from 'app/shared/model/band-member.model';

@Injectable({ providedIn: 'root' })
export class BandMemberResolve implements Resolve<IBandMember> {
  constructor(private service: BandMemberService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBandMember> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BandMember>) => response.ok),
        map((bandMember: HttpResponse<BandMember>) => bandMember.body)
      );
    }
    return of(new BandMember());
  }
}

export const bandMemberRoute: Routes = [
  {
    path: '',
    component: BandMemberComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.bandMember.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BandMemberDetailComponent,
    resolve: {
      bandMember: BandMemberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.bandMember.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BandMemberUpdateComponent,
    resolve: {
      bandMember: BandMemberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.bandMember.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BandMemberUpdateComponent,
    resolve: {
      bandMember: BandMemberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.bandMember.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const bandMemberPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BandMemberDeletePopupComponent,
    resolve: {
      bandMember: BandMemberResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'monoApp.bandMember.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
