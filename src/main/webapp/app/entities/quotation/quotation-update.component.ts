import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IQuotation, Quotation } from 'app/shared/model/quotation.model';
import { QuotationService } from './quotation.service';
import { IUser, UserService } from 'app/core';
import { IBand } from 'app/shared/model/band.model';
import { BandService } from 'app/entities/band';

@Component({
  selector: 'jhi-quotation-update',
  templateUrl: './quotation-update.component.html'
})
export class QuotationUpdateComponent implements OnInit {
  quotation: IQuotation;
  isSaving: boolean;

  users: IUser[];

  bands: IBand[];

  editForm = this.fb.group({
    id: [],
    date: [],
    name: [],
    lastName: [],
    address: [],
    phone: [],
    email: [],
    observation: [null, [Validators.maxLength(4096)]],
    userQuotationId: [],
    bandQuotationsId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected quotationService: QuotationService,
    protected userService: UserService,
    protected bandService: BandService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ quotation }) => {
      this.updateForm(quotation);
      this.quotation = quotation;
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bandService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBand[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBand[]>) => response.body)
      )
      .subscribe((res: IBand[]) => (this.bands = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(quotation: IQuotation) {
    this.editForm.patchValue({
      id: quotation.id,
      date: quotation.date != null ? quotation.date.format(DATE_TIME_FORMAT) : null,
      name: quotation.name,
      lastName: quotation.lastName,
      address: quotation.address,
      phone: quotation.phone,
      email: quotation.email,
      observation: quotation.observation,
      userQuotationId: quotation.userQuotationId,
      bandQuotationsId: quotation.bandQuotationsId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const quotation = this.createFromForm();
    if (quotation.id !== undefined) {
      this.subscribeToSaveResponse(this.quotationService.update(quotation));
    } else {
      this.subscribeToSaveResponse(this.quotationService.create(quotation));
    }
  }

  private createFromForm(): IQuotation {
    const entity = {
      ...new Quotation(),
      id: this.editForm.get(['id']).value,
      date: this.editForm.get(['date']).value != null ? moment(this.editForm.get(['date']).value, DATE_TIME_FORMAT) : undefined,
      name: this.editForm.get(['name']).value,
      lastName: this.editForm.get(['lastName']).value,
      address: this.editForm.get(['address']).value,
      phone: this.editForm.get(['phone']).value,
      email: this.editForm.get(['email']).value,
      observation: this.editForm.get(['observation']).value,
      userQuotationId: this.editForm.get(['userQuotationId']).value,
      bandQuotationsId: this.editForm.get(['bandQuotationsId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IQuotation>>) {
    result.subscribe((res: HttpResponse<IQuotation>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackBandById(index: number, item: IBand) {
    return item.id;
  }
}
