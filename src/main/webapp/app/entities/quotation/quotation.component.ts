import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IQuotation } from 'app/shared/model/quotation.model';
import { AccountService } from 'app/core';
import { QuotationService } from './quotation.service';

@Component({
  selector: 'jhi-quotation',
  templateUrl: './quotation.component.html'
})
export class QuotationComponent implements OnInit, OnDestroy {
  quotations: IQuotation[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected quotationService: QuotationService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.quotationService
      .query()
      .pipe(
        filter((res: HttpResponse<IQuotation[]>) => res.ok),
        map((res: HttpResponse<IQuotation[]>) => res.body)
      )
      .subscribe(
        (res: IQuotation[]) => {
          this.quotations = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInQuotations();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IQuotation) {
    return item.id;
  }

  registerChangeInQuotations() {
    this.eventSubscriber = this.eventManager.subscribe('quotationListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
