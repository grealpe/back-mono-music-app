import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { MonoSharedModule } from 'app/shared';
import {
  QuotationComponent,
  QuotationDetailComponent,
  QuotationUpdateComponent,
  QuotationDeletePopupComponent,
  QuotationDeleteDialogComponent,
  quotationRoute,
  quotationPopupRoute
} from './';

const ENTITY_STATES = [...quotationRoute, ...quotationPopupRoute];

@NgModule({
  imports: [MonoSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    QuotationComponent,
    QuotationDetailComponent,
    QuotationUpdateComponent,
    QuotationDeleteDialogComponent,
    QuotationDeletePopupComponent
  ],
  entryComponents: [QuotationComponent, QuotationUpdateComponent, QuotationDeleteDialogComponent, QuotationDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MonoQuotationModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
