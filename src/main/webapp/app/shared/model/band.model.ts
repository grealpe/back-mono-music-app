import { IBandMember } from 'app/shared/model/band-member.model';
import { IQuotation } from 'app/shared/model/quotation.model';
import { ICategory } from 'app/shared/model/category.model';
import { IImages } from 'app/shared/model/images.model';

export interface IBand {
  id?: number;
  name?: string;
  video?: string;
  description?: string;
  star?: number;
  members?: IBandMember[];
  quotations?: IQuotation[];
  categories?: ICategory[];
  images?: IImages[];
}

export class Band implements IBand {
  constructor(
    public id?: number,
    public name?: string,
    public video?: string,
    public description?: string,
    public star?: number,
    public members?: IBandMember[],
    public quotations?: IQuotation[],
    public categories?: ICategory[],
    public images?: IImages[]
  ) {}
}
