import { IBand } from 'app/shared/model/band.model';

export interface ICategory {
  id?: number;
  name?: string;
  image?: string;
  bandCategories?: IBand[];
}

export class Category implements ICategory {
  constructor(public id?: number, public name?: string, public image?: string, public bandCategories?: IBand[]) {}
}
