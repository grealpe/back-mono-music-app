import { IBand } from 'app/shared/model/band.model';

export interface IImages {
  id?: number;
  url?: string;
  bandImages?: IBand[];
}

export class Images implements IImages {
  constructor(public id?: number, public url?: string, public bandImages?: IBand[]) {}
}
