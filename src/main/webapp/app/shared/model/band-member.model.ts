export interface IBandMember {
  id?: number;
  bandusermembersFirstName?: string;
  bandusermembersId?: number;
  bandMembersName?: string;
  bandMembersId?: number;
}

export class BandMember implements IBandMember {
  constructor(
    public id?: number,
    public bandusermembersFirstName?: string,
    public bandusermembersId?: number,
    public bandMembersName?: string,
    public bandMembersId?: number
  ) {}
}
