import { Moment } from 'moment';

export interface IQuotation {
  id?: number;
  date?: Moment;
  name?: string;
  lastName?: string;
  address?: string;
  phone?: string;
  email?: string;
  observation?: string;
  userQuotationFirstName?: string;
  userQuotationId?: number;
  bandQuotationsName?: string;
  bandQuotationsId?: number;
}

export class Quotation implements IQuotation {
  constructor(
    public id?: number,
    public date?: Moment,
    public name?: string,
    public lastName?: string,
    public address?: string,
    public phone?: string,
    public email?: string,
    public observation?: string,
    public userQuotationFirstName?: string,
    public userQuotationId?: number,
    public bandQuotationsName?: string,
    public bandQuotationsId?: number
  ) {}
}
